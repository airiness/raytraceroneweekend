#include <fstream>
#include <iostream>
#include "Sphere.h"
#include "Hitable_list.h"
#include "float.h"
#include "Camera.h"
#include "material.h"
#include "Lambertian.h"
#include "Metal.h"
#include "Dielectric.h"
using namespace std;

Vec3 color(const Ray& r, Hitable* world, int depth)
{
	hit_record rec;
	if (world->hit(r, 0.001, FLT_MAX, rec))
	{
		Ray scattered;
		Vec3 attenuation;
		if (depth < 100 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation * color(scattered, world, depth + 1);
		}
		else
		{
			return Vec3(0.0f, 0.0f, 0.0f);
		}
	}
	else
	{
		Vec3 unit_direction = unit_vector(r.Direction());
		float t = 0.5f * (unit_direction.y() + 1.0f);
		return (1.0f - t) * Vec3(1.0f, 1.0f, 1.0f) + t * Vec3(0.5f, 0.7f, 1.0f);
	}
}

Hitable* random_scene() {
	int n = 500;
	Hitable** list = new Hitable * [n + 1];
	list[0] = new Sphere(Vec3(0, -1000, 0), 1000, new Lambertian(Vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++) {
		for (int b = -11; b < 11; b++) {
			float choose_mat = Rand::drand48();
			Vec3 center(a + 0.9f * Rand::drand48(), 0.2f, b + 0.9f * Rand::drand48());
			if ((center - Vec3(4.0f, 0.2f, 0.0f)).length() > 0.9f) {
				if (choose_mat < 0.8f) {  // diffuse
					list[i++] = new Sphere(center, 0.2f, new Lambertian(Vec3(Rand::drand48() * Rand::drand48(), Rand::drand48() * Rand::drand48(), Rand::drand48() * Rand::drand48())));
				}
				else if (choose_mat < 0.95f) { // metal
					list[i++] = new Sphere(center, 0.2f,
						new Metal(Vec3(0.5f * (1.0f + Rand::drand48()), 0.5f * (1.0f + Rand::drand48()), 0.5f * (1.0f + Rand::drand48())), 0.5f * Rand::drand48()));
				}
				else {  // glass
					list[i++] = new Sphere(center, 0.2f, new Dielectric(1.5));
				}
			}
		}
	}

	list[i++] = new Sphere(Vec3(0, 1, 0), 1.0, new Dielectric(1.5));
	list[i++] = new Sphere(Vec3(-4, 1, 0), 1.0, new Lambertian(Vec3(0.4f, 0.2f, 0.1f)));
	list[i++] = new Sphere(Vec3(4, 1, 0), 1.0, new Metal(Vec3(0.7f, 0.6f, 0.5f), 0.0f));

	return new Hitable_list(list, i);
}


void DrawPic()
{
	std::fstream fppm("fppm.ppm", std::ios::out);
	int nx = 1920;
	int ny = 1080;
	int ns = 10;
	fppm << "P3\n" << nx << " " << ny << "\n255\n";
	//cout << "P3\n" << nx << " " << ny << "\n255\n";

	//Hitable* list[5];
	//list[0] = new Sphere(Vec3(0, 0, -1), 0.5, new Lambertian(Vec3(0.8f, 0.4f, 0.3f)));
	//list[1] = new Sphere(Vec3(0, -100.5, -1), 100,new Lambertian(Vec3(0.2f,0.8f,0.8f)));
	//list[2] = new Sphere(Vec3(1, 0, -1), 0.5,new Metal(Vec3(0.3f,0.6f,0.2f),0.0f));
	//list[3] = new Sphere(Vec3(-1, 0, -1), 0.5,new Dielectric(1.5f));
	//list[4] = new Sphere(Vec3(-1, 0, -1), -0.45,new Dielectric(1.5f));
	//Hitable* world = new Hitable_list(list, 5);
	Hitable* world = random_scene();
	Vec3 lookfrom(15, 2, 3);
	Vec3 lookat(0, 0, 0);
	float dist_to_focus = (lookfrom - lookat).length();
	float aperture = 0.1f;
	Camera cam(lookfrom, lookat, Vec3(0, 1, 0), 20, float(nx) / float(ny), aperture, dist_to_focus);
	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			Vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + Rand::drand48()) / float(nx);
				float v = float(j + Rand::drand48()) / float(ny);
				Ray r = cam.get_ray(u, v);
				Vec3 p = r.point_at_parameter(2.0);
				col += color(r, world, 0);
			}
			col /= float(ns);
			col = Vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99 * col[0]);
			int ig = int(255.99 * col[1]);
			int ib = int(255.99 * col[2]);
			fppm << ir << " " << ig << " " << ib << "\n";
			//cout << ir << " " << ig << " " << ib << "\n";
		}
	}
	fppm.close();
}

int main()
{
	DrawPic();
	return true;
}

