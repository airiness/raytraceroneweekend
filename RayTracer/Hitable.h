#pragma once
#include"Ray.h"
class Material;

struct hit_record
{
	float t = 0.0f;		//ray's t
	Vec3 p;			//the point ray hit surface
	Vec3 normal;	//normal  vec of hit point
	Material* mat_ptr = nullptr;
};

class Hitable
{
public:
	virtual bool hit(const Ray& r, float t_min, float t_max, hit_record& rec) const = 0;
};
