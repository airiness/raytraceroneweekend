#pragma once
#include "Material.h"

class Dielectric : public Material
{
public:
	Dielectric(float ri) :ref_idx(ri) {}

	virtual bool scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const override
	{
		//Vec3 outward_normal;
		//Vec3 reflected = reflect(r_in.Direction(), rec.normal);
		//float ni_over_nt;
		//attenuation = Vec3(1.0, 1.0, 1.0);
		//Vec3 refracted;
		//float reflect_prob;
		//float cosine;
		//if (dot(r_in.Direction(), rec.normal) > 0) {
		//	outward_normal = -rec.normal;
		//	ni_over_nt = ref_idx;
		//	//         cosine = ref_idx * dot(r_in.Direction(), rec.normal) / r_in.Direction().length();
		//	cosine = dot(r_in.Direction(), rec.normal) / r_in.Direction().length();
		//	cosine = sqrt(1 - ref_idx * ref_idx * (1 - cosine * cosine));
		//}
		//else {
		//	outward_normal = rec.normal;
		//	ni_over_nt = 1.0f / ref_idx;
		//	cosine = -dot(r_in.Direction(), rec.normal) / r_in.Direction().length();
		//}
		//if (refract(r_in.Direction(), outward_normal, ni_over_nt, refracted))
		//	reflect_prob = schlick(cosine, ref_idx);
		//else
		//	reflect_prob = 1.0;
		//if (Rand::drand48() < reflect_prob)
		//	scattered = Ray(rec.p, reflected);
		//else
		//	scattered = Ray(rec.p, refracted);
		//return true;


		Vec3 outward_normal;
		Vec3 reflected = reflect(r_in.Direction(), rec.normal);
		float ni_over_nt;
		attenuation = Vec3(1.0f, 1.0f, 1.0f);
		Vec3 refracted;
		float reflect_prob;
		float cosine;
		if (dot(r_in.Direction(), rec.normal) > 0)
		{
			outward_normal = -rec.normal;
			ni_over_nt = ref_idx;
			cosine = ref_idx + dot(r_in.Direction(), rec.normal) / r_in.Direction().length();
		}
		else
		{
			outward_normal = rec.normal;
			ni_over_nt = 1.0f / ref_idx;
			cosine = -dot(r_in.Direction(), rec.normal) / r_in.Direction().length();
		}
		if (refract(r_in.Direction(), outward_normal, ni_over_nt, refracted))
		{
			reflect_prob = schlick(cosine, ref_idx);
		}
		else
		{
			scattered = Ray(rec.p, reflected);
			reflect_prob = 1.0f;
		}
		if (Rand::drand48() < reflect_prob)
		{
			scattered = Ray(rec.p, reflected);
		}
		else
		{
			scattered = Ray(rec.p, refracted);
		}

		return true;
	}

	float ref_idx;
};