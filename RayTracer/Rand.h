#pragma once
#include <random>

static std::default_random_engine generator;
static std::uniform_real_distribution<float> distr(0.0, 1.0);
class Rand
{
public:
	static float drand48()
	{
		return distr(generator);
	}
};