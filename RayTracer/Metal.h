#pragma once
#include "Material.h"

class Metal :public Material
{
public:
	Metal(const Vec3& a, float f) : albedo(a) { if (f < 1)fuzz = f; else fuzz = 1; }

	virtual bool scatter(const Ray& r_in, const hit_record& rec, Vec3& attenuation, Ray& scattered) const override
	{
		Vec3 reflected = reflect(unit_vector(r_in.Direction()), rec.normal);
		scattered = Ray(rec.p, reflected + fuzz * random_in_unit_sphere());
		attenuation = albedo;
		return (dot(scattered.Direction(), rec.normal) > 0);
	}

	Vec3 albedo;
	float fuzz;
};