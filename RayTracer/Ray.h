#pragma once
#include "Vec3.h"

class Ray
{
public:
	Ray() {};

	Ray(const Vec3& a, const Vec3& b) :O(a), D(b) {};
	Vec3 Origin() const { return O; }
	Vec3 Direction() const { return D; }
	//the point when give the t
	Vec3 point_at_parameter(float t) const { return O + t * D; };
	Vec3 O;
	Vec3 D;
};

